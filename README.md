# README #

### CS 441 - P08 ###

### Commander in Tweet! ###
*Summary: Play as the POTUS by doing what he does best: avoiding facts at all cost! Use the left and right arrows to avoid the facts falling from the sky. If you make contact with a fact, the game is over. For every fact you avoid, your score increases by one. For every tweet you send out (collect), your score increases by 5. 

### Who do I talk to? ###

* Ciaran Slattery
* Binghamton University - CS 441