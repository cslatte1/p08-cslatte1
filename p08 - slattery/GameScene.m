//
//  GameScene.m
//  p08 - slattery
//
//  Created by Ciaran Slattery on 5/8/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

#import "GameScene.h"

@interface GameScene (){
    
}

@end


@implementation GameScene {
    bool lose;
    SKSpriteNode *leftButton, *rightButton, *resetButton;
    SKLabelNode *scoreLabel, *highScoreLabel, *loseLabel;
    NSInteger score, highScore;
    SKAction *factSpawn, *moveRight, *moveLeft, *runLeft, *runRight, *jump, *stop, *moveLeftTrump, *moveRightTrump, *factFall;
    Trump *trump;
    SKSpriteNode *floor, *leftBound, *rightBound, *ceiling;
    CGPoint tweetLocations[6];
    CGPoint factLocations[16];
}


//Collision categories
static const uint32_t factCategory = 1 << 0;
static const uint32_t trumpCategory = 1 << 1;
static const uint32_t tweetCategory = 1 << 2;
static const uint32_t boundCategory = 1 << 3;
static const uint32_t floorCategory = 1 << 4;
static const uint32_t ceilingCategory = 1 << 5;

- (void)didMoveToView:(SKView *)view {
    // Setup your scene here
    [self initData];
    [self initEnvironment];
    [self initScoring];
    [self initButtons];
    
    self.physicsWorld.contactDelegate = self;
    
    [self createTrump];
    [self createFact];
    [self createTweets];
    
}


- (void)initData{
    
    lose = false;
    
    moveLeft = [SKAction moveBy:CGVectorMake(-850000, 0) duration:5000];
    moveRight = [SKAction moveBy:CGVectorMake(850000, 0) duration:5000];
    moveLeftTrump = [SKAction moveBy:CGVectorMake(-2000000, 0) duration:5000];
    moveRightTrump = [SKAction moveBy:CGVectorMake(2000000, 0) duration:5000];
    
    //Trump movement animation: Left
    SKTexture *trumpRunLeft = [SKTexture textureWithImageNamed:@"Trump"];
    trumpRunLeft.filteringMode = SKTextureFilteringNearest;
    runLeft = [SKAction setTexture:trumpRunLeft];
    
    //Trump movement animation: Right
    SKTexture *trumpRunRight = [SKTexture textureWithImageNamed:@"Trump"];
    trumpRunRight.filteringMode = SKTextureFilteringNearest;
    runRight = [SKAction setTexture:trumpRunRight];
    
    //Fact falling animation
    SKTexture *factFalling = [SKTexture textureWithImageNamed:@"Fact"];
    factFalling.filteringMode = SKTextureFilteringNearest;
    factFall = [SKAction setTexture:factFalling];
    
    // Initialize gravity
    self.physicsWorld.gravity = CGVectorMake(0.0f, -1.0f);
    
    tweetLocations[0] = CGPointMake(0, -50);
    tweetLocations[1] = CGPointMake(150, -50);
    tweetLocations[2] = CGPointMake(-150, -50);
    tweetLocations[3] = CGPointMake(100, -150);
    tweetLocations[4] = CGPointMake(-100, -150);
    tweetLocations[5] = CGPointMake(300, 50);
    
    factLocations[0] = CGPointMake(-340, 180);
    factLocations[1] = CGPointMake(-300, 180);
    factLocations[2] = CGPointMake(-250, 180);
    factLocations[3] = CGPointMake(-200, 180);
    factLocations[4] = CGPointMake(-150, 180);
    factLocations[5] = CGPointMake(-100, 180);
    factLocations[6] = CGPointMake(-50, 180);
    factLocations[7] = CGPointMake(0, 180);
    factLocations[8] = CGPointMake(50, 180);
    factLocations[9] = CGPointMake(100, 180);
    factLocations[10] = CGPointMake(150, 180);
    factLocations[11] = CGPointMake(200, 180);
    factLocations[12] = CGPointMake(250, 180);
    factLocations[13] = CGPointMake(300, 180);
    factLocations[14] = CGPointMake(320, 180);
    factLocations[15] = CGPointMake(340, 180);
}


-(void) initEnvironment{
    //Set background color
    self.backgroundColor = [SKColor grayColor];
    
    SKTexture *platform = [SKTexture textureWithImageNamed:@"platform"];
    platform.filteringMode = SKTextureFilteringNearest;

    // Create bottom borders (will contain movement, shoot, and jump buttons)
    floor = [SKSpriteNode spriteNodeWithTexture:platform];
    [floor setXScale:0.47];
    [floor setYScale:0.35];
    floor.position = CGPointMake(0, -201);
    floor.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:floor.size];
    floor.physicsBody.restitution = 0.0f;
    floor.physicsBody.linearDamping = 0.1f;
    floor.physicsBody.friction = 0.2f;
    floor.physicsBody.dynamic = false;
    floor.physicsBody.categoryBitMask = floorCategory;
    floor.physicsBody.collisionBitMask = factCategory | trumpCategory | tweetCategory;
    floor.physicsBody.contactTestBitMask = factCategory | trumpCategory | tweetCategory;
    
    [self addChild:floor];
    
    
    SKTexture *leftBoundary = [SKTexture textureWithImageNamed:@"sideBound"];
    leftBoundary.filteringMode = SKTextureFilteringNearest;
    
    // Create bottom borders (will contain movement, shoot, and jump buttons)
    leftBound = [SKSpriteNode spriteNodeWithTexture:leftBoundary];
    [leftBound setXScale:0.25];
    [leftBound setYScale:10.0];
    leftBound.position = CGPointMake(-375, 0);
    leftBound.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:leftBound.size];
    leftBound.physicsBody.restitution = 0.0f;
    leftBound.physicsBody.linearDamping = 0.1f;
    leftBound.physicsBody.friction = 0.2f;
    leftBound.physicsBody.dynamic = false;
    leftBound.physicsBody.categoryBitMask = boundCategory;
    leftBound.physicsBody.collisionBitMask = trumpCategory;
    leftBound.physicsBody.contactTestBitMask = trumpCategory;
    
    [self addChild:leftBound];
    
    SKTexture *rightBoundary = [SKTexture textureWithImageNamed:@"sideBound"];
    rightBoundary.filteringMode = SKTextureFilteringNearest;
    
    // Create bottom borders (will contain movement, shoot, and jump buttons)
    rightBound = [SKSpriteNode spriteNodeWithTexture:rightBoundary];
    [rightBound setXScale:0.25];
    [rightBound setYScale:10.0];
    rightBound.position = CGPointMake(375, 0);
    rightBound.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:rightBound.size];
    rightBound.physicsBody.restitution = 0.0f;
    rightBound.physicsBody.linearDamping = 0.1f;
    rightBound.physicsBody.friction = 0.2f;
    rightBound.physicsBody.dynamic = false;
    rightBound.physicsBody.categoryBitMask = boundCategory;
    rightBound.physicsBody.collisionBitMask = trumpCategory;
    rightBound.physicsBody.contactTestBitMask = trumpCategory;
    
    [self addChild:rightBound];
}

-(void) initAssets{
    /*
    // Init lose label
    loseLabel = [SKLabelNode labelNodeWithText:@""];
    loseLabel.fontSize = 75;
    loseLabel.fontName = @"Futura-CondensedExtraBold";
    loseLabel.fontColor = [SKColor redColor];
    loseLabel.position = CGPointMake(0, 0);
    loseLabel.text = [NSString stringWithFormat:@"YOU LOSE"];
    loseLabel.hidden = 1;
     
    [self addChild:loseLabel];*/
}

- (void)initScoring {
    // Initialize high score
    
    highScore = [[[NSUserDefaults standardUserDefaults] objectForKey:@"HighScore"] intValue ];
    highScoreLabel = [SKLabelNode labelNodeWithText:@""];
    highScoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    highScoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    highScoreLabel.position = CGPointMake(280, 201);
    highScoreLabel.fontSize = 22;
    highScoreLabel.fontName = @"Futura-CondensedExtraBold";
    highScoreLabel.fontColor = [SKColor whiteColor];
    highScoreLabel.text = [NSString stringWithFormat:@"HS: %04ld", (long)highScore];
    
    [self addChild:highScoreLabel];
    
    // Initialize current score
    score = 0;
    scoreLabel = [SKLabelNode labelNodeWithText:@""];
    scoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    scoreLabel.position = CGPointMake(-375, 201);
    scoreLabel.fontSize = 22;
    scoreLabel.fontName = @"Futura-CondensedExtraBold";
    scoreLabel.fontColor = [SKColor whiteColor];
    scoreLabel.text = [NSString stringWithFormat:@"S: %04ld", (long)score];
    
    [self addChild:scoreLabel];
}

-(void) initButtons{
    
    SKTexture *Rbutton = [SKTexture textureWithImageNamed:@"RButton"];
    Rbutton.filteringMode = SKTextureFilteringNearest;
    rightButton = [SKSpriteNode spriteNodeWithTexture:Rbutton];
    rightButton.position = CGPointMake(330, -178);
    [rightButton setScale:0.1f];
    rightButton.zPosition = 1;
    rightButton.alpha = .8;
    [self addChild:rightButton];
    
    SKTexture *Lbutton = [SKTexture textureWithImageNamed:@"LButton"];
    Lbutton.filteringMode = SKTextureFilteringNearest;
    leftButton = [SKSpriteNode spriteNodeWithTexture:Lbutton];
    leftButton.position = CGPointMake(-330, -178);
    [leftButton setScale:0.1f];
    leftButton.zPosition = 1;
    leftButton.alpha = .8;
    [self addChild:leftButton];

    SKTexture *loseText = [SKTexture textureWithImageNamed:@"Lose"];
    resetButton = [SKSpriteNode spriteNodeWithTexture:loseText];
    [resetButton setScale:1.0];
    resetButton.zPosition = 1;
    //resetButton.alpha = .4;
    resetButton.hidden = YES;
    [self addChild:resetButton];
}


-(void) createTrump {
    trump = [Trump spriteNodeWithImageNamed:@"Trump"];
    [trump setScale:0.25];
    trump.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:trump.size];
    
    // Restitution sets bounciness
    trump.physicsBody.restitution = 0.0f;
    
    // Linear dampening is like friction
    trump.physicsBody.linearDamping = 0.1f;
    
    trump.physicsBody.friction = 0.1f;
    trump.physicsBody.categoryBitMask = trumpCategory;
    trump.physicsBody.collisionBitMask = floorCategory;
    trump.physicsBody.contactTestBitMask = factCategory | tweetCategory | floorCategory;
    trump.physicsBody.usesPreciseCollisionDetection = YES;
    trump.physicsBody.allowsRotation = NO;
    trump.position = CGPointMake(0, -85);
    trump.xScale = trump.xScale * -1;
    
    
    // Start hero facing right
    trump.direction = 1;
    
    [self addChild:trump];
    [trump runAction:stop withKey:@"stop"];
}

-(void) createFact {
    
    Fact* enemy;
    enemy = [Fact spriteNodeWithImageNamed:@"Fact"];
    [enemy runAction:factFall];
    [enemy setScale:0.15f];
    //enemy.health = 1;
    
    enemy.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:enemy.size];
    enemy.physicsBody.restitution = 0.0f;
    enemy.physicsBody.linearDamping = 0.1f;
    enemy.physicsBody.friction = 0.2f;
    enemy.physicsBody.usesPreciseCollisionDetection = YES;
    enemy.physicsBody.allowsRotation = NO;
    
    enemy.physicsBody.categoryBitMask = factCategory;
    enemy.physicsBody.collisionBitMask = 0;
    enemy.physicsBody.contactTestBitMask = trumpCategory | floorCategory | ceilingCategory;
    
    int random = arc4random_uniform(16);
    
    //Randomly chooses poisition from which facts fall
    switch(random){
        case 0:
            enemy.position = factLocations[0];
            break;
        case 1:
            enemy.position = factLocations[1];
            break;
        case 2:
            enemy.position = factLocations[2];
            break;
        case 3:
            enemy.position = factLocations[3];
            break;
        case 4:
            enemy.position = factLocations[4];
            break;
        case 5:
            enemy.position = factLocations[5];
            break;
        case 6:
            enemy.position = factLocations[6];
            break;
        case 7:
            enemy.position = factLocations[7];
            break;
        case 8:
            enemy.position = factLocations[8];
            break;
        case 9:
            enemy.position = factLocations[9];
            break;
        case 10:
            enemy.position = factLocations[10];
            break;
        case 11:
            enemy.position = factLocations[11];
            break;
        case 12:
            enemy.position = factLocations[12];
            break;
        case 13:
            enemy.position = factLocations[13];
            break;
        case 14:
            enemy.position = factLocations[14];
            break;
        case 15:
            enemy.position = factLocations[15];
            break;
        default:
            NSLog(@"Received %d for fact random number, when it should be 0-15", random);
    }
    
    [self addChild:enemy];
    
    [NSTimer scheduledTimerWithTimeInterval:0.75 target:self selector:@selector(createFact) userInfo:nil repeats:NO];
    
}

-(void) createTweets{
    SKSpriteNode *tweet = [SKSpriteNode spriteNodeWithImageNamed:@"Tweet"];
    [tweet setScale:0.10f];
    tweet.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:tweet.size];

    tweet.physicsBody.restitution = 0.0f;
    tweet.physicsBody.linearDamping = 0.1f;
    tweet.physicsBody.friction = 0.2f;
    tweet.physicsBody.usesPreciseCollisionDetection = YES;
    tweet.physicsBody.allowsRotation = NO;
    tweet.physicsBody.categoryBitMask = tweetCategory;
    tweet.physicsBody.collisionBitMask = 0;
    tweet.physicsBody.contactTestBitMask = trumpCategory | floorCategory;
    
    int random = arc4random_uniform(16);
    
    //Randomly chooses poisition from which facts fall
    switch(random){
        case 0:
            tweet.position = factLocations[0];
            break;
        case 1:
            tweet.position = factLocations[1];
            break;
        case 2:
            tweet.position = factLocations[2];
            break;
        case 3:
            tweet.position = factLocations[3];
            break;
        case 4:
            tweet.position = factLocations[4];
            break;
        case 5:
            tweet.position = factLocations[5];
            break;
        case 6:
            tweet.position = factLocations[6];
            break;
        case 7:
            tweet.position = factLocations[7];
            break;
        case 8:
            tweet.position = factLocations[8];
            break;
        case 9:
            tweet.position = factLocations[9];
            break;
        case 10:
            tweet.position = factLocations[10];
            break;
        case 11:
            tweet.position = factLocations[11];
            break;
        case 12:
            tweet.position = factLocations[12];
            break;
        case 13:
            tweet.position = factLocations[13];
            break;
        case 14:
            tweet.position = factLocations[14];
            break;
        case 15:
            tweet.position = factLocations[15];
            break;
        default:
            NSLog(@"Received %d for tweet random number, when it should be 0-15", random);
    }
    
    [self addChild:tweet];
    
    [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(createTweets) userInfo:nil repeats:NO];
}

- (void)touchDownAtPoint:(CGPoint)pos {
    
}

- (void)touchMovedToPoint:(CGPoint)pos {
    
}

- (void)touchUpAtPoint:(CGPoint)pos {
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    CGPoint touchLocation = [[touches anyObject] locationInNode:self];
    
    if(!lose) {

        if([rightButton containsPoint:touchLocation]){
            [trump runAction:moveRightTrump withKey:@"moveRight"];
            [trump removeActionForKey:@"stop"];
            [trump runAction:runRight withKey:@"runRight"];
            if(trump.direction == 0)
                trump.xScale = trump.xScale * -1;
            trump.direction = 1;
            
            rightButton.alpha = .3;
        }
        if([leftButton containsPoint:touchLocation]){
            [trump runAction:moveLeftTrump withKey:@"moveLeft"];
            [trump removeActionForKey:@"stop"];
            [trump runAction:runLeft withKey:@"runLeft"];
            if(trump.direction == 1)
                trump.xScale = trump.xScale * -1;
            trump.direction = 0;
            
            leftButton.alpha = .3;
        }
    }
    else {
        if([resetButton containsPoint:touchLocation]) {
            [self resetGame];
        }
    }
}

-(void) didBeginContact:(SKPhysicsContact *)contact{
    
    SKPhysicsBody *firstBody, *secondBody;
    
    if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask){
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    } else {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    //Trump collects a tweet
    if(firstBody.categoryBitMask == trumpCategory && secondBody.categoryBitMask == tweetCategory){
        //Delete tweet
        [secondBody.node removeFromParent];
        score = score+5;
        scoreLabel.text = [NSString stringWithFormat:@"S: %04ld", (long)score];
        NSLog(@"score: %04ld", (long)score);
    }
    
    //Trump killed by fact
    if(firstBody.categoryBitMask == factCategory && secondBody.categoryBitMask == trumpCategory){
       NSLog(@"Ya made it");
       [self loseGame];
       NSLog(@"lose value: %d", lose);
    }
    
    //Score increases by 1 for every fact Trump avoids
    if(firstBody.categoryBitMask == factCategory && secondBody.categoryBitMask == floorCategory){
        score = score+1;
        scoreLabel.text = [NSString stringWithFormat:@"S: %04ld", (long)score];
        NSLog(@"score: %04ld", (long)score);
    }
    
    //Trump hits edge of screen
    if(firstBody.categoryBitMask == trumpCategory && secondBody.categoryBitMask == boundCategory){
        [trump removeActionForKey:@"moveRight"];
        [trump removeActionForKey:@"runRight"];
        
        [trump removeActionForKey:@"moveLeft"];
        [trump removeActionForKey:@"runLeft"];
    }

    
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    /* Called when a touch moves */
    CGPoint touchLocation = [[touches anyObject] locationInNode:self];
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch ends */
    CGPoint touchLocation = [[touches anyObject] locationInNode:self];
    
    if([rightButton containsPoint:touchLocation]){
        [trump removeActionForKey:@"moveRight"];
        [trump removeActionForKey:@"runRight"];
        [trump runAction:stop withKey:@"stop"];
        rightButton.alpha = .8;
    }
    if([leftButton containsPoint:touchLocation]){
        [trump removeActionForKey:@"moveLeft"];
        [trump removeActionForKey:@"runLeft"];
        [trump runAction:stop withKey:@"stop"];
        leftButton.alpha = .8;
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch is cancelled (may not need) */
    CGPoint touchLocation = [[touches anyObject] locationInNode:self];
    
}


-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
    if(lose){
        [self checkHighScore];
    }
    
    
}

- (void)resetGame {
    lose = false;
    //loseLabel.hidden = 1;
    resetButton.hidden = 1;
    score = 0;
    scoreLabel.text = [NSString stringWithFormat:@"S: %04ld", (long)score];
    trump.position = CGPointMake(0, -20);
    [self addChild:trump];
}

- (void)loseGame {
    NSLog(@"AYYYY");
    lose = true;
    //loseLabel.hidden = 0;
    resetButton.hidden = NO;
    [self checkHighScore];
    [trump removeFromParent];
}


- (void)setHighScore {
    highScore = score;
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:(int)score] forKey:@"HighScore"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    highScoreLabel.text = [NSString stringWithFormat:@"HS: %04ld", (long)highScore];
}

- (void)checkHighScore {
    if(score > highScore) {
        [self setHighScore];
    }
}
@end
