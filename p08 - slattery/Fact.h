//
//  Fact.h
//  p08 - slattery
//
//  Created by Ciaran Slattery on 5/9/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Fact: SKSpriteNode
@property (atomic) bool direction;
@property (atomic) int health;


@end