//
//  GameScene.h
//  p08 - slattery
//
//  Created by Ciaran Slattery on 5/8/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Trump.h"
#import "Fact.h"

@interface GameScene : SKScene<SKPhysicsContactDelegate>
@end